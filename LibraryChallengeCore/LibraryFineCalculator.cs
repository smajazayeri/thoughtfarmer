﻿using System;
using System.Collections.Generic;

namespace LibraryChallengeCore
{
    public class LibraryBookFineCalculator : ILibraryBookFineCalculator
    {

        /// <summary>
        /// Based on todays date calculate the fine for each book and return the total fine due for all of the books.
        /// Some books may not be currently checked out or may not be overdue
        /// 
        /// Rules
        /// 
        /// The fine for an individual book is calculated using the following rule 
        /// 
        /// 1. The library is closed on Sundays so no fees are calculated for Sundays
        /// 2. For days 1 - 10 the fine per day is 0.10
        /// 3. For days 11 - 20 the fine per day is 0.13
        /// 4. For days 21 - 50 the fine per day is 0.17
        /// 5. For days 51+ is 1% of the total fine increasing by 0.3% every day 
        ///     - e.g. day 51 1% of the current total, day 52 1.3% of the current total, day 53 1.6% for the current total
        ///     - sundays still result in a 0 fine, but do increase the percentage amount
        /// 6. Wednesdays are "double fine" day. If the current day is a Wednesday then the fine for that day is double. Double fine day does not apply once the book is in the 51+ days range
        /// 7. If the book is "non-fiction" then is gets a 25% discount on its total fine
        ///  
        /// </summary>
        /// <param name="today">The date to check the books against to calculate the fine</param>
        /// <param name="books">A list of library books</param>
        /// <returns></returns>

        const float DISCOUNT = 0.25f, 
            INITIAL_COEFFICIENT = 0.01f, COEFFICIENT_STEP = 0.003f,
            FIRST_LEVEL_FINE = 0.1f, SECOND_LEVEL_FINE = 0.13f, THIRD_LEVEL_FINE = 0.17f;

        public decimal CalculateTotalFine(DateTime today, List<ILibraryBook> books)
        {
            decimal totalFine = 0;
            try
            {
                Decimal discount, individualFine;
                foreach (LibraryBook book in books)
                {
                    if (book.DueDate != null)
                    {
                        DateTime due = book.DueDate.Value;
                        individualFine = CalculateIndividualFine(today, due);
                        discount = GetDiscount(book.Category, individualFine);
                        totalFine += individualFine - Convert.ToDecimal(discount);
                    }
                }
            }
            catch (System.InvalidOperationException e)
            {
                System.Console.WriteLine(e.Message);
            }
            return Math.Truncate(100 * totalFine) / 100;
        }

        public decimal CalculateIndividualFine(DateTime today, DateTime due)
        {
            decimal fine = 0;
            float coefficient = 0, dailyFine = 0;
            int dayCount = 1;
            bool is51Plus = false;
            for (DateTime dt = due.AddDays(1.0); dt <= today; dt = dt.AddDays(1.0), dayCount++)
            {
                float wednesdayImpact = 1;
                if (dt.DayOfWeek == DayOfWeek.Sunday)
                {
                    if (is51Plus)
                        coefficient += COEFFICIENT_STEP;
                    continue;
                }
                else if (dt.DayOfWeek == DayOfWeek.Wednesday && !is51Plus) {
                    wednesdayImpact = 2;
                }
                if (dayCount >= 1 && dayCount <= 10)
                {
                    dailyFine = FIRST_LEVEL_FINE * wednesdayImpact;
                }
                else if (dayCount >= 11 && dayCount <= 20)
                {
                    dailyFine = SECOND_LEVEL_FINE * wednesdayImpact;
                }
                else if (dayCount >= 21 && dayCount <= 50)
                {
                    dailyFine = THIRD_LEVEL_FINE * wednesdayImpact;
                }
                else if (dayCount > 50)
                {
                    if (dayCount == 51)
                    {
                        is51Plus = true;
                        coefficient = INITIAL_COEFFICIENT;
                    }
                    else
                        coefficient += COEFFICIENT_STEP;
                    dailyFine = coefficient * (float)fine;
                }

                fine += Convert.ToDecimal(dailyFine);
            }
            return fine;
        }

        public Decimal GetDiscount(LibraryBookCategory category, Decimal fine)
        {
            if (category == LibraryBookCategory.Biography)
                return fine * Convert.ToDecimal(DISCOUNT);
            return 0;
        }

    }
}