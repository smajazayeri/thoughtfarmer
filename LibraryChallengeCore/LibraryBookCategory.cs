﻿
namespace LibraryChallengeCore
{
    public enum LibraryBookCategory
    {
        Biography = 0,
        Humour = 1,
        Mystery = 2,
        Romance = 3,
        Scifi = 4,
    };
}
