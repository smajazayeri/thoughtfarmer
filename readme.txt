Installation guide
------------------

1) Run the server using Visual Studio
2) Open a command prompt (e.g., Terminal on Mac, CMD on Windows)
3) Navigate to the LibraryChallengeWeb > React
3) run npm install to install the required dependencies
4) run the task2.html in the React folder on a server (In your JS IDE (e.g., WebStorm), simply right click on the
task2.html file and select "Run".


User guide
------------------
1) In the rendered application, type a category name.
2) For more deails, click on the top right icon
3) The row colors mean as expected:
   - Green: means that due is in the future
   - Red: means that it's overdue
   - Orange: means that today is due