var React = require('react'),
    _ = require('underscore'),
    Table = require('./Table.jsx'),
    request = require('request');

var LibraryApp = React.createClass({

    getInitialState: function () {
        return {
            books: [],
            balance: 0,
            category: ''
        };
    },

    _capitalizeString: function(string) {
        return string.charAt(0).toUpperCase() + string.slice(1);
    },

    _searchByCategory: function() {
        var self = this,
            categoryName = this._capitalizeString(this.refs.category.getDOMNode().value);
        if (categoryName) {
            //call to the api
            request('http://localhost:52301/api/library/books/' + categoryName, function (error, response, body) {
                if (!error && response.statusCode == 200) {
                    self.setState({
                        books: self._polishForComponents(JSON.parse(body)),
                        category: categoryName
                    });
                    console.log(body);
                } else {
                    alert(error);
                    console.error('Invalid response');
                }
            });
            request('http://localhost:52301/api/library/fines/' + categoryName, function (error, response, body) {
                console.log(body);
                if (!error && response.statusCode == 200) {
                    self.setState({
                        balance: JSON.parse(body)
                    });
                } else {
                    console.error(error);
                }
            });
        } else {
            alert('You have to type the category name first');
        }
    },

    _polishForComponents: function(json) {
        var self = this, books = [];
        _.forEach(json, function(element) {
            var book = {
                title: element.title,
                author: element.author,
                isbn: element.isbn,
                due: self._getDate(element.dueDate)
            };
            books.push(book);
        })
        return books;
    },

    _getDate(date) {
        //date is 2016-02-06T04:35:30.8965749-08:00
        if (date && date.length > 10) {
            return date.substring(0, 10)   //2016-02-06
        }
        return date;
    },

    render: function () {
        var categoryInput =
            <div className='input-group'>
                <input type='text' className='form-control' ref='category'
                       placeholder='Category name (e.g., mystery)' required='required'/>
                    <span className='input-group-btn'>
                        <button className='btn btn-primary' type='button' onClick={this._searchByCategory}>
                            Search</button>
                    </span>
            </div>;
        var bookList = <Table books={this.state.books}
                              balance={this.state.balance}
                              category={this.state.category}/>;

        return (
            <div>
                {categoryInput}
                {bookList}
            </div>
        );
    }
});

React.render(<LibraryApp />, document.getElementById('content'));