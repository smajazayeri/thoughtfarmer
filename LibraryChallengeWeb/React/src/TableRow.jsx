var React = require('react'),
    _ = require('underscore'),
    moment = require('moment'),
    Icon = require('./Icon.jsx');

const FULL_WIDTH_HEADER = 1, REGULAR_HEADER = 2, NOT_HEADER = 3;

var TableRow = React.createClass({

    propTypes: {
        data: React.PropTypes.object.isRequired,
        level: React.PropTypes.number.isRequired,
        index: React.PropTypes.number,
        callback: React.PropTypes.func
    },

    getDefaultProps: function() {
        return {
            data: {},
            level: NOT_HEADER,
            index: 0
        };
    },

    _setRowColor() {
        //sets the row color using the Bootstrap classes: success (g), warning (o), danger (r)
        var book = this.props.data.value;
        if (book) {
            var due = moment(book.due, 'YYYY-MM-DD');
            if (moment().isSame(due, 'day')) {
                return 'warning';
            } else if (moment().isBefore(due)) {
                return 'success';
            } else if (moment().isAfter(due)) {
                return 'danger';
            }
        }
    },

    render: function () {
        var columns,
            rowClassName,
            props = this.props,
            data = props.data,
            level = props.level;

        switch (level) {
            case FULL_WIDTH_HEADER: {
                //category info bar
                var booksInCategory = data.count,
                    categoryName = data.value,
                    balance = data.balance,
                    categoryInfo = [];
                rowClassName = 'active';
                categoryInfo.push(<th key='category-name' colSpan='2'>{'Category: ' + categoryName}</th>);
                categoryInfo.push(<th key='book-count'>{'# of Books: ' + booksInCategory}</th>);
                categoryInfo.push(<th key='balance'>{'Balance: ' + balance}</th>);
                categoryInfo.push(<th key='icon' className="text-right">
                    <Icon classname='glyphicon glyphicon-th' title='Show/Hide' callback={props.callback}/></th>);
                columns = categoryInfo;
                break;
            }
            case REGULAR_HEADER: {
                //title bar
                columns = data.value.map(function(column, index) {
                    return <th key={index}>{column}</th>;
                });
                break;
            }
            default: {
                //data columns
                var bookColmuns = [],
                    num = props.index + 1;
                bookColmuns.push(<td key={'num-' + num}>{num}</td>);
                _.forEach(data.value, function(column, index) {
                    bookColmuns.push(<td key={index + '-' + num}>{column}</td>);
                });
                rowClassName = this._setRowColor();
                columns = bookColmuns;
            }
        }
        return <tr className={rowClassName}>{columns}</tr>;
    }
});

module.exports = TableRow;