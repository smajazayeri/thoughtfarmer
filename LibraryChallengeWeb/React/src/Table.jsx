var React = require('react'),
    TableRow = require('./TableRow.jsx');
const FULL_WIDTH_HEADER = 1, REGULAR_HEADER = 2, NOT_HEADER = 3;

var Table = React.createClass({

    propTypes: {
        books: React.PropTypes.array.isRequired,
        category: React.PropTypes.string
    },

    getDefaultProps: function() {
        return {
            books: [],
            category: ''
        };
    },

    getInitialState: function () {
        return {
            showRow: false
        };
    },

    _toggleTable: function() {
        //This function shows/hides the table by clicking on
        //the icon of the category info bar
        this.setState({showRow: !this.state.showRow});
    },

    render: function () {
        var books = this.props.books,
            booksCount = books.length,
            category = this.props.category,
            balance = this.props.balance;

        var list = false;

        if (category) { // category name is submitted
            var categoryObj = {value: category, count: booksCount, balance: balance},
                columnTitles = {value: ['#', 'Title', 'Author', 'ISBN', 'Due Date']};
            var categoryInfoBar = <TableRow data={categoryObj} level={FULL_WIDTH_HEADER} callback={this._toggleTable}/>;
            var titleBar = false, dataColumns = false;

            if (this.state.showRow) {
                titleBar = <TableRow data={columnTitles} level={REGULAR_HEADER}/>;
                dataColumns = books.map(function (book, index) {
                    var bookObject = {value: book};
                    return <TableRow data={bookObject}
                                     level={NOT_HEADER}
                                     index={index}
                                     key={'row-' + index}/>;
                });
            }

            list = <div className='table-responsive'>
                <table className='table'>
                    <thead>
                        {categoryInfoBar}
                        {titleBar}
                    </thead>
                    <tbody>
                        {dataColumns}
                    </tbody>
                </table>
            </div>;
        }

       return list;
    }
});

module.exports = Table;