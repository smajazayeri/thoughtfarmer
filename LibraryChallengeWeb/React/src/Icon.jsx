var React = require('react');

var Icon = React.createClass({

    propTypes: {
        classname: React.PropTypes.string.isRequired,
        title: React.PropTypes.string,
        callback: React.PropTypes.func
    },

    getDefaultProps: function() {
        return {
            classname: ''
        };
    },

    render: function () {
        var classname = this.props.classname,
            title = this.props.title,
            callback = this.props.callback;
        return <span className={classname} style={{cursor: 'pointer'}} title={title} onClick={callback}></span>;
    }
});

module.exports = Icon;