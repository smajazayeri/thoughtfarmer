'use strict';

var ReactTools = require('react-tools');
module.exports = {
    process: function(src, path) {
        if (!/\.jsx$/.test(path)) {
            return src;
        }
        return ReactTools.transform(src, { harmony: true, stripTypes: true });
    }
};