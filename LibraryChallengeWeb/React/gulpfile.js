'use strict';

var gulp = require('gulp');
var browserify = require('browserify');
var source = require('vinyl-source-stream');
var reactify = require('reactify');
var foreach = require('gulp-foreach');
var watchify = require('watchify');
var notify = require('gulp-notify');
var path = require('path');
var clean = require('gulp-clean');
var uglify = require('gulp-uglify');
var runSequence = require('run-sequence');
var rename = require('gulp-rename');
var gulpPrint = require('gulp-print');
var underscore = require('underscore');
var gutil = require('gulp-util');
var reactTools = require('react-tools');
var intercept = require('gulp-intercept');
var os = require('os');

function handleErrors() {
    var args = Array.prototype.slice.call(arguments);
    notify.onError({
        title: 'Compile Error',
        message: '<%= error.message %>'
    }).apply(this, args);
    this.emit('end'); // Keep gulp from hanging on this task
}

function processScripts(filepath, watch) {
    var basename = path.basename(filepath);
    var appname = basename.substring(0, basename.length - 'App.jsx'.length);

    var bundler, rebundle;
    bundler = browserify(filepath, {
        debug: true,
        cache: {}, // required for watchify
        packageCache: {}, // required for watchify
        fullPaths: watch // required to be true only for watchify
    });

    if (watch) {
        bundler = watchify(bundler);
    }

    bundler.transform(reactify);

    rebundle = function () {
        var target = 'bundle-' + appname.toLowerCase() + '.js';

        gutil.log('Building ' + basename + ' to ' + target);

        var stream = bundler.bundle();

        // the errors should not interrupt the process only in watch mode
        if (watch) {
            stream.on('error', handleErrors);
        }

        return stream.pipe(source(target))
            .pipe(gulp.dest('./build'));
    };

    bundler.on('update', rebundle);
    return rebundle();
}

gulp.task('watch', function () {
    return gulp.src('./src/**/*App.jsx')
        .pipe(foreach(function (stream, file) {
            return processScripts(file.path, true);
        }));
});

/*
 * Clean the build directories containing the generated files/packages.
 */
gulp.task('clean', function () {
    /** remove all from the build directory, except the README file */
    return gulp
        .src(
            ['./build/*'],
            {read: false})
        .pipe(clean({force: true}));
});

/*
 * Build the bundles, one per application.
 */
gulp.task('bundle', function () {
    return gulp.src('./src/**/*App.jsx')
        .pipe(foreach(function (stream, file) {
            return processScripts(file.path, false);
        }));
});

/*
 * Clean the project and build and compress the packages.
 */
gulp.task('package', function () {
    runSequence('clean', 'bundle');
});

/*
 * Run all the tasks
 */
gulp.task('default', function () {
    runSequence('package');
});
